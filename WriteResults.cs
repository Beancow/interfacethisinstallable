using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GraphQL;

using interfacethisinstallable.Models;
using interfacethisinstallable.Config;

namespace interfacethisinstallable
{
    public class WriteResponseToJson
    {
        static GraphQLResponse<Data> response {get; set;}
        public WriteResponseToJson(GraphQLResponse<Data> response)
        {

            foreach (var item in response.Data.aisubscriptions) 
            { 
                if (File.Exists(@"" 
                    + InterfaceThisAppSettings.filesConvertDirectory
                    + "/"
                    + item.docid
                    + "." 
                    + item.patientrecord.image.imagename.Split('.')[1])) 
                {
                    File.Delete(@"" 
                    + InterfaceThisAppSettings.filesConvertDirectory
                    + "/"
                    + item.docid
                    + "." 
                    + item.patientrecord.image.imagename.Split('.')[1]);    
                    Console.WriteLine( item.docid
                    + "." 
                    + item.patientrecord.image.imagename.Split('.')[1] + " deleted");
                }
                if (File.Exists(@"" 
                    + InterfaceThisAppSettings.filesInDirectory
                    + "/"
                    + item.docid
                    + ".jpg")) 
                {
                    File.Delete(@"" 
                    + InterfaceThisAppSettings.filesInDirectory
                    + "/"
                    + item.docid
                    + ".jpg");     
                    Console.WriteLine( item.docid
                    + ".jpg deleted");
                } 

                File.WriteAllBytes(@"" 
                    + InterfaceThisAppSettings.filesConvertDirectory
                    + "/"
                    + item.docid
                    + "." 
                    + item.patientrecord.image.imagename.Split('.')[1],
                    Convert.FromBase64String(item.patientrecord.image.imagedata.Split(',')[1]));
                
                new ImageWriterTick(InterfaceThisAppSettings.filesConvertDirectory 
                                    + "/" 
                                    + item.docid 
                                    + "."
                                    + item.patientrecord.image.imagename.Split('.')[1]);

                StreamWriter file = File.CreateText(@""+ InterfaceThisAppSettings.filesOutDirectory 
                                                      + "/" 
                                                      + item.docid 
                                                      + ".json");
                JsonTextWriter writer = new JsonTextWriter(file);

                byte[] imageArray = System.IO.File.ReadAllBytes(@""+ InterfaceThisAppSettings.filesInDirectory 
                                                                   + "/" 
                                                                   + item.docid 
                                                                   + ".jpg");
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);

                string json = @"{'ResultData': { 'docid':'"
                                + item.docid + "',"
                                + "'token':'"
                                + InterfaceThisAppSettings.clientID + "',"
                                + "'testresone':'" 
                                + item.patientrecord.testresone + "',"
                                + "'testrestwo':'" 
                                + item.patientrecord.testrestwo + "',"
                                + "'image': { 'imagename':'" 
                                + item.patientrecord.image.imagename + "'," 
                                + "'imagedata': 'data:image/jpg;base64," 
                                + base64ImageRepresentation + "'"
                                + "}}}";
                
                JObject jsonToWrite = JObject.Parse(json);
                jsonToWrite.WriteTo(writer);
                writer.Close();
                file.Close();

                Console.WriteLine();

                Console.WriteLine("This Document: " + item.docid);
                Console.WriteLine("First name: " + item.patientrecord.firstname);
                Console.WriteLine("Surname: " + item.patientrecord.surname);
                Console.WriteLine("Result 1: " + item.patientrecord.testresone);
                Console.WriteLine("Result 2: " + item.patientrecord.testrestwo);
                foreach (var sub in item.patientrecord.subscriptions) 
                { 
                    Console.WriteLine("Subscribed to: " + sub);
                }

                Console.WriteLine();
            }
        }
    }
}