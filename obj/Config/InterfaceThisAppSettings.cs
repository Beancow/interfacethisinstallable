
namespace interfacethisinstallable.Config
{
    public class InterfaceThisAppSettings
    {
        public static string interfaceThisCLientEndpoint 
        { 
            get 
            {
                return "https://us-central1-interfacethisio.cloudfunctions.net/api/graphql";
            }
        }
        public static string filesInDirectory 
        { 
            get 
            {
                return "filesIn";
            }
        }

        public static string filesConvertDirectory 
        { 
            get 
            {
                return "filesConvert";
            }
        }

        public static string filesOutDirectory 
        { 
            get 
            {
                return "filesOut";
            }
        }

        public static string clientID 
        { 
            get 
            {
                return "12345";
            }
        }
    }
}
