using System.Collections.Generic;

namespace interfacethisinstallable.Models
{
    public class Patient
    {
        public string firstname {set; get;}
        public string surname {set; get;}
        public string testresone {set; get;}
        public string testrestwo {set; get;}
        public List<string> subscriptions = new List<string>();
        public ImageContent image { set; get; }  
    }
}