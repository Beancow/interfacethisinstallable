using System;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;

using interfacethisinstallable.Config;

namespace interfacethisinstallable
{
    public class InterfaceThisGrapQLCLient
    {
        static readonly Lazy<GraphQLHttpClient> _clientHolder = new Lazy<GraphQLHttpClient>(CreateGraphQLClient);
        static GraphQLHttpClient Client => _clientHolder.Value;
        static GraphQLHttpClient CreateGraphQLClient() {

            var options = new GraphQLHttpClientOptions
            {
                EndPoint = new Uri(InterfaceThisAppSettings.interfaceThisCLientEndpoint),
            #if !DEBUG
                HttpMessageHandler = new ModernHttpClient.NativeMessageHandler()
            #endif
            };

            return new GraphQLHttpClient(options, new NewtonsoftJsonSerializer());
        }
        public static GraphQLHttpClient dbClient { get{
            return InterfaceThisGrapQLCLient.Client;
            } 
        }
    }
}