using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using GraphQL;

using interfacethisinstallable.Models;
using interfacethisinstallable.Config;


namespace interfacethisinstallable
{
    class SendToGraphQL
        :InterfaceThisGrapQLCLient
    {

        public static async Task SendData(string docid)
        {
        JObject jObject = JObject.Parse(File.ReadAllText(@"" 
            + InterfaceThisAppSettings.filesOutDirectory 
            + "/" 
            + docid));

        Console.Write(jObject);
        var mutation = new GraphQLRequest
            {
                Query = @"mutation($ResultData: ResultInput) {
                addResultsFromAI(ResultData: $ResultData){
                    testresone
                    testrestwo
                    image {
                        imagename
                    }
                }
                }",
                Variables = jObject
            };

        var response = await InterfaceThisGrapQLCLient.dbClient.SendMutationAsync<ReturnToSender>(mutation);
            Console.Write(response);
        }
    }
}
