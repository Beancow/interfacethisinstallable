using System.Drawing;

using interfacethisinstallable.Config;

namespace interfacethisinstallable
{

    class ImageWriterTick
    {   
        public ImageWriterTick(string pathtofile)
        {   
            Image imageBackground = Image.FromFile(pathtofile);
            Image imageOverlay = Image.FromFile("tick.png");

            int imageOutWidth = imageBackground.Width;
            int imageOutHeight = imageBackground.Height;
            
            if (imageOutWidth >= 200 && imageOutWidth < 400  ||
                 imageOutHeight >= 300 && imageOutHeight < 600) 
            {
                imageOutWidth = imageOutWidth / 2;
                imageOutHeight = imageOutHeight / 2;
            }

            if (imageOutWidth >= 400 || imageOutHeight >= 600) 
            {
                imageOutWidth = imageOutWidth / 6;
                imageOutHeight = imageOutHeight / 6;
            }

            Image img = new Bitmap(imageOutWidth, imageOutWidth );
            using (Graphics gr = Graphics.FromImage(img))
            {
                gr.DrawImage(imageBackground, 0, 0, imageOutWidth, imageOutHeight);
                gr.DrawImage(imageOverlay, 0, 0, imageOutWidth, imageOutHeight);
            }
            int splitCount = pathtofile.Split("/").Length -1;
            string splitName = pathtofile.Split("/")[splitCount];
            img.Save(InterfaceThisAppSettings.filesInDirectory + "/" + splitName.Split(".")[0] +".jpg");
        }
    }
}