
using System.Threading.Tasks;
using GraphQL;


using interfacethisinstallable.Models;
using interfacethisinstallable.Config;


namespace interfacethisinstallable
{
    class GetFromGraphql
        :InterfaceThisGrapQLCLient
    {
        public static async Task GetData()
        {

        var jObject = new Newtonsoft.Json.Linq.JObject();
        jObject.Add("token", InterfaceThisAppSettings.clientID);

        var query = new GraphQLRequest
            {
                Query = @" query($token: String){ 
                aisubscriptions(token: $token){
                    docid
                    patientrecord
                    {
                        firstname
                        surname
                        testresone
                        testrestwo
                        subscriptions
                        image { 
                            imagename
                            imagedata
                            }
                        }
                    }
                }",
                Variables = jObject
            };

        var response = await InterfaceThisGrapQLCLient.dbClient.SendQueryAsync<Data>(query);

        new WriteResponseToJson(response);

        }
    }
}
