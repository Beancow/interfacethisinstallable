﻿using System;
using System.IO;
using System.Timers;
using System.Threading.Tasks;

using interfacethisinstallable.Config;
namespace interfacethisinstallable
{
    class Program
        :GetFromGraphql
    {
        private static System.Timers.Timer dataTransferTimer;

        static void Main()
        {
            System.IO.Directory.CreateDirectory(InterfaceThisAppSettings.filesInDirectory);
            System.IO.Directory.CreateDirectory(InterfaceThisAppSettings.filesOutDirectory);
            System.IO.Directory.CreateDirectory(InterfaceThisAppSettings.filesConvertDirectory);

                    
            dataTransferTimer = new System.Timers.Timer(30000);

            dataTransferTimer.Elapsed += executeDataTransfer;
            dataTransferTimer.AutoReset = true;
            dataTransferTimer.Enabled = true;
        
            Console.ReadKey();

        }
        private static void executeDataTransfer(Object source, ElapsedEventArgs e)
        {
            var t = SendDataNow();
            
                t.Wait();
                if (t.Status == TaskStatus.Faulted) 
                {
                    Console.WriteLine("Task Error");
                    Console.WriteLine();
                } 

                while(!t.IsCompleted)
                {
                    Console.WriteLine("Send Complete");
                    Console.WriteLine();
                }
        }
        static async Task SendDataNow()
        {
            await GetData();
            
            string [] fileEntries = Directory.GetFiles("filesOut/", "*.json");
            
            foreach(string fileName in fileEntries)
            {
                var t = SendToGraphQL.SendData(fileName.Split("/")[1]);

                t.Wait();
                if (t.Status == TaskStatus.Faulted) 
                {
                    Console.WriteLine("Task Error");
                    Console.WriteLine();
                } 

                while(!t.IsCompleted)
                {
                    Console.WriteLine("Send Complete");
                    Console.WriteLine();
                }
            }
        }
    }
}
