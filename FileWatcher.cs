using System;
using System.IO;
using System.Threading;

namespace interfacethisinstallable
{

    class FileWatcher
        :SendToGraphQL
    {
        public FileWatcher()
        {
            MonitorDirectory();
        }
        public static void MonitorDirectory()
        {

            FileSystemWatcher fileSystemWatcher = new FileSystemWatcher();

            fileSystemWatcher.Path = "filesOut/";

            fileSystemWatcher.Created += FileSystemWatcher_Created;

            fileSystemWatcher.Renamed += FileSystemWatcher_Renamed;

            fileSystemWatcher.Deleted += FileSystemWatcher_Deleted;

            fileSystemWatcher.EnableRaisingEvents = true;

        }

        private static async void FileSystemWatcher_Created(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("File created: {0}", e.Name);
            await SendData(e.Name);
        }

        private static void FileSystemWatcher_Renamed(object sender, FileSystemEventArgs e)
        {

            Console.WriteLine("File renamed: {0}", e.Name);

        }

        private static void FileSystemWatcher_Deleted(object sender, FileSystemEventArgs e)
        {

            Console.WriteLine("File deleted: {0}", e.Name);

        }
    }
}